import math
from PIL import Image, ImageDraw, ImageFont
from sponge_case import spongeCase
from split_lines import splitLines
from get_x import getx

def makeImageMacro(str):
  """
    Generates the image macro from a template image and a string

    Args:
      str (str): String to attach to the template
    Returns:
      img (Image): Rendered meme
  """

  # Import image and save its properties
  img = Image.open('img/template.jpg')
  img_w, img_h = img.size
  # Import the font
  font = ImageFont.truetype(font='font/impact.ttf', size=54)
  lineHeight = font.getsize('hg')[1] * 1.1
  # Spongecase the input and get the lines
  lines = splitLines(spongeCase(str), font, img_w * 0.975)

  # Draw text onto the template
  draw = ImageDraw.Draw(img)
  fill = 'rgb(255,255,255)'
  strokeColor = 'rgb(0,0,0)'
  strokeWidth = 3
  topLine = 5
  bottomLine = img_h - 5 - lineHeight
  numLines = len(lines)
  # If there's only one line, it goes on the bottom
  if numLines == 1:
    draw.text((getx(font, img_w, lines[0]), bottomLine), lines[0], fill=fill, font=font, stroke_fill=strokeColor, stroke_width=strokeWidth)
  # If there's more than one, we split the list into a top list and a bottom list, overloading the bottom
  else:
    numTop = math.floor(numLines / 2)
    numBot = numLines - numTop
    y = topLine
    i = 0
    while i < numTop:
      draw.text((getx(font, img_w, lines[i]), y), lines[i], fill=fill, font=font, stroke_fill=strokeColor, stroke_width=strokeWidth)
      y += lineHeight
      i += 1
    y = bottomLine - (lineHeight * (numBot - 1))
    while i < numLines:
      draw.text((getx(font, img_w, lines[i]), y), lines[i], fill=fill, font=font, stroke_fill=strokeColor, stroke_width=strokeWidth)
      y += lineHeight
      i += 1

  img.show()
  return img

# Allow to run in the command line as 
if __name__ == '__main__':
  import sys
  sys.argv.pop(0)
  makeImageMacro(' '.join(sys.argv))
  