# sPoNgEcAsE Generator

Some [gatekeeper](https://old.reddit.com/r/gatekeeping) said I have to learn Python to be a real developer.
So I decided to expand on a [dumb script](https://gitlab.com/snippets/1952441) I whipped up ages ago to
talk smack in the group-chat. This takes a string, converts it to sPoNgEcAsE and applies it to the
finest-quality specimen of that classic image I could find with 10 seconds of Googling.

<img src="https://gitlab.com/saveriodesign/spongecase/-/raw/master/img/sample.jpg" alt="Sample SpongeCase" width="460">

It even automatically breaks up the lines in such a way that they're sort of the same length, and prevents
upper-case "i" or lowercase "L" from making the result completely illegible. Even supports accented characters,
vOuS pOuVeZ dOnC L'uTiLiSeR pOuR pArLeR La MeRdE eN fRaNçAiS. These make it better than the
[official implementation](https://itnext.io/how-to-wrap-text-on-image-using-python-8f569860f89e) over on
memegenerator, which will 100% let upper-case "î" sneak through, disgusting.
