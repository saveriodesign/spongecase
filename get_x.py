def getx(font, width, str):
  """
    Calculate the x-position for the textbox that will result in centered text

    Args:
      font (FreeTypeFont): Desired font
      width (int): Image width
      str (str): Text to center
    Returns:
      x (float): X-coordinate of the left-side of the text box
  """

  return (width - font.getsize(str)[0]) / 2
