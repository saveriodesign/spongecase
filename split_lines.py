import math

def splitLines(str, font, maxWidth):
  """
    Split the given text into separate, equal-ish lines

    Args:
      str (str): Text to wrap
      font (FreeTypeFont): Font being used
      maxWidth (int): Maximum pixel width
    Returns:
      lines (list[str]): List of resolved lines
  """

  lines = []

  # If the full text is smaller than the width, just return it
  fullTextWidth = font.getsize(str)[0]
  if fullTextWidth <= maxWidth:
    lines.append(str)

  else:

    # Break up words
    words = str.split(' ')
    # Determine number of lines needed
    numLines = math.ceil(fullTextWidth / maxWidth)
    # Determine the break-point where we'll start a new line, such that all the lines will have equal-ish length
    nominalLineWidth = fullTextWidth / numLines

    # Build up our lines by looping through the words
    line = ''
    i = 0
    l = len(words)
    j = l - 1
    while i < l:
      testLine = '{0} {1}'.format(line, words[i]).strip()
      testLength = font.getsize(testLine)[0]
      # last word
      if i == j:
        lines.append(testLine)
        i += 1
      # too long
      elif testLength > maxWidth:
        lines.append(line)
        line = ''
      # too short
      elif testLength < nominalLineWidth:
        line = testLine
        i += 1
      # just right
      else:
        lines.append(testLine)
        line = ''
        i += 1

  return lines
