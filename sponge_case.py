import random

r = ''
i = True

def spongeCase(str):
  
  """
    Bastardizes a string into "sPoNgEcAsE". Takes care to ensure all instances
    of "i" and "L" are lower- and upper-case, respectively to prevent illegibility.
    Supports accented instances of those letters. Also ignores numbers, whitespace,
    and common punctuation.

    Args:
      str (str): String to ruin
    Returns:
      mock (str): MoCkiNg StRiNg iN sPoNgEcAsE
  """

  global r
  global i
  r = ''
  i = random.randint(0,1) == 0

  # Build a switch statement from scratch because Python is shit
  def alwaysLower(char):
    global r
    global i
    r += char.lower()
    i = False
    return False
  def alwaysUpper(char):
    global r
    global i
    r += char.upper()
    i = True
    return False
  def ignore(char):
    global r
    r += char
  def alternate(char):
    global r
    global i
    r += (char.upper(), char.lower())[i]
    i = (True, False)[i]

  switch = {

    # "I" looks dumb in uppercase
    'i': alwaysLower,
    'ì': alwaysLower,
    'í': alwaysLower,
    'î': alwaysLower,
    'ĩ': alwaysLower,
    'ī': alwaysLower,
    'ĭ': alwaysLower,
    'į': alwaysLower,
    'ı': alwaysLower,
    'ĳ': alwaysLower,
    'ǐ': alwaysLower,
    'ȉ': alwaysLower,
    'ȋ': alwaysLower,
    'ɨ': alwaysLower,
    'ᶖ': alwaysLower,
    'ᶤ': alwaysLower,
    'ḭ': alwaysLower,
    'ḯ': alwaysLower,

    # "L" looks dumb in lowercase
    'l': alwaysUpper,
    'ĺ': alwaysUpper,
    'ļ': alwaysUpper,
    'ľ': alwaysUpper,
    'ŀ': alwaysUpper,
    'ł': alwaysUpper,
    'ᶪ': alwaysUpper,
    'ᶩ': alwaysUpper,
    'ᶅ': alwaysUpper,
    'ḷ': alwaysUpper,
    'ḹ': alwaysUpper,
    'ḻ': alwaysUpper,
    'ḽ': alwaysUpper,

    # Ignore common non-letters when toggling
    ' ' : ignore,
    '\'': ignore,
    '.' : ignore,
    ',' : ignore,
    '?' : ignore,
    '!' : ignore,
    '~' : ignore,
    '`' : ignore,
    '-' : ignore,
    '"' : ignore,
    ':' : ignore,
    ';' : ignore,
    '(' : ignore,
    ')' : ignore,
    '[' : ignore,
    ']' : ignore,
    '{' : ignore,
    '}' : ignore,
    '0' : ignore,
    '1' : ignore,
    '2' : ignore,
    '3' : ignore,
    '4' : ignore,
    '5' : ignore,
    '6' : ignore,
    '7' : ignore,
    '8' : ignore,
    '9' : ignore,
    '+' : ignore,
    '=' : ignore,
    '<' : ignore,
    '>' : ignore,
    '@' : ignore,
    '#' : ignore,
    '$' : ignore,
    '%' : ignore,
    '^' : ignore,
    '&' : ignore,
    '*' : ignore,
    '€' : ignore,
    '§' : ignore,
    '¢' : ignore,
    '£' : ignore,
    '₿' : ignore

  }

  # Build and return the string
  for c in str.lower():
    f = switch.get(c, lambda char: True)
    if (f(c)):
      alternate(c)
  return r

if __name__ == '__main__':
  import sys
  sys.argv.pop(0)
  print(spongeCase(' '.join(sys.argv)))
